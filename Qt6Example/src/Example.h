#include <QArrayData>
#include <QMainWindow>
#include <QLineEdit>

class Window1 : public QMainWindow {
  Q_OBJECT
public:
  Window1() {
    lineEdit1.setText("paste here:");
    lineEdit1.move(10, 10);

    setCentralWidget(&frame);
    setWindowTitle("Right click clipboard bug example");
    resize(300, 300);
  }

private:
  QFrame frame;
  QLineEdit lineEdit1 {&frame};
};
